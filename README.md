### How do I get set up? ###
To run application set up database environment in application.properties file.
Also you may set up initial data using script from application_db.sql
All dependencies are in pom.xml file
Angular dependencies are in package.json
