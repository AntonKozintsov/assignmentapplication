import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Assignment} from "../modules/models/assignment";
import {timeout} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AssignmentService {

  constructor(private http: HttpClient) {
  }

  getAssignments(): Observable<Assignment[]> {
    return this.http.get<Assignment[]>("administrator/assignments");
  }

  applyAssignment(assignmentId: string): Observable<Assignment> {
    return this.http.post<Assignment>('/administrator/assignments/apply/' + assignmentId, assignmentId);
  }

  declineAssignment(assignmentId: string): Observable<Assignment> {
    return this.http.post<Assignment>('/administrator/assignments/decline/' + assignmentId, assignmentId);
  }

  saveAssignment(assignment: Assignment): Observable<Assignment> {
    return this.http.post<Assignment>('/administrator/assignments/save', assignment);
  }

  deleteAssignment(assignmentId: string): Observable<void> {
    return this.http.delete<void>('/administrator/assignments/delete/' + assignmentId);
  }
  getAssignmentsClient(): Observable<Assignment[]> {
    return this.http.get<Assignment[]>("/client/assignments").pipe(timeout(2000));
  }
  saveAssignmentClient(assignment: Assignment): Observable<Assignment> {
    return this.http.post<Assignment>('/client/assignments/save', assignment);
  }
  checkStatusClient(assignmentId: string): Observable<Assignment> {
    return this.http.get<Assignment>('/client/assignments/status/' + assignmentId);
  }
}
