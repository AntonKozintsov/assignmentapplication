import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {ModalModule} from "ngx-bootstrap/modal";
import {FormsModule} from "@angular/forms";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarComponent} from './modules/navbar/navbar.component';
import {HomeComponent} from './modules/home/home.component';
import {AssignmentsComponent} from './modules/assignments/assignments.component';
import {NotFoundComponent} from './modules/not-found/not-found.component';
import {ClientAssignmentsComponent} from './modules/client-assignments/client-assignments.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AssignmentsComponent,
    NotFoundComponent,
    ClientAssignmentsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ModalModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
