export class Assignment {
  id: String;
  request: String;
  bid: String;
  due_date: String;
  status: String;

  static cloneAssignment(assignment: Assignment): Assignment {
    const clonedAssignment: Assignment = new Assignment();
    clonedAssignment.id = assignment.id;
    clonedAssignment.request = assignment.request;
    clonedAssignment.bid = assignment.bid;
    clonedAssignment.due_date = assignment.due_date;
    clonedAssignment.status = assignment.status;
    return clonedAssignment;
  }


}
