import {Component, OnInit, TemplateRef} from '@angular/core';
import {Assignment} from "../models/assignment";
import {Subscription} from "rxjs";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {AssignmentService} from "../../services/assignment.service";
import {Ng4LoadingSpinnerService} from "ng4-loading-spinner";

@Component({
  selector: 'app-client-assignments',
  templateUrl: './client-assignments.component.html',
  styleUrls: ['./client-assignments.component.css']
})
export class ClientAssignmentsComponent implements OnInit {

  public editMode = false;


  public assignments: Assignment[];

  private subscriptions: Subscription[] = [];

  public status: string;

  public editableAssignment: Assignment = new Assignment();

  public modalRef: BsModalRef; //we need a variable to keep a reference of our modal. This is going to be used to close the modal.

  constructor(private assignmentService: AssignmentService,
              private modalService: BsModalService) {}

  ngOnInit() {
    this.loadAssignments();
  }

  public addAssignment(): void {
    this.subscriptions.push(this.assignmentService.saveAssignmentClient(this.editableAssignment).subscribe(() => {
      this.loadAssignments();
      this.closeModal();
      this.refreshAssignment() ;
    }));

  }

  private loadAssignments(): void {
    this.subscriptions.push(this.assignmentService.getAssignmentsClient().subscribe(assignments=> {
      this.assignments = assignments as Assignment[];
    }));
  }
  public openModal(template: TemplateRef<any>, assignment : Assignment): void {
    if (assignment) {
      this.editMode = true;
      this.editableAssignment = Assignment.cloneAssignment(assignment);
    } else {
      this.refreshAssignment();
      this.editMode = false;
    }
    this.modalRef = this.modalService.show(template);
  }
  public openStatus(template: TemplateRef<any>, assignmentId : string): void {
    this.subscriptions.push(this.assignmentService.checkStatusClient(assignmentId).subscribe(assignment => {
      this.editableAssignment = assignment;
    }));
    this.modalRef = this.modalService.show(template);

  }

  public closeModal(): void {
    this.modalRef.hide();
  }


  private refreshAssignment(): void {
    this.editableAssignment = new Assignment();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

}
