import {Component, OnInit, TemplateRef} from '@angular/core';
import {Assignment} from "../models/assignment";
import {Subscription} from "rxjs";
import {AssignmentService} from "../../services/assignment.service";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
@Component({
  selector: 'app-assignments',
  templateUrl: './assignments.component.html',
  styleUrls: ['./assignments.component.css']
})
export class AssignmentsComponent implements OnInit {

  public appliedAssignments: Number;

  public declinedAssignments: Number;

  public assignments: Assignment[];

  public editMode = false;

  private subscriptions: Subscription[] = [];

  public statisticsMode = false;

  public editableAssignment: Assignment = new Assignment();


  public modalRef: BsModalRef; //we need a variable to keep a reference of our modal. This is going to be used to close the modal.

  constructor(private assignmentService: AssignmentService,
              private modalService: BsModalService) { }
  ngOnInit() {
    this.loadAssignments();
  }

  public addAssignment(): void {
    this.subscriptions.push(this.assignmentService.saveAssignment(this.editableAssignment).subscribe(() => {
      this.loadAssignments();
      this.refreshAssignment()  ;
      this.closeModal();
    }));
  }

  public deleteAssignment(assignmentId: string): void {
    this.subscriptions.push(this.assignmentService.deleteAssignment(assignmentId).subscribe(() => {
      this.loadAssignments();
    }));
  }
  public applyAssignment(assignmentId: string): void {
    this.subscriptions.push(this.assignmentService.applyAssignment(assignmentId).subscribe(() => {
      this.loadAssignments();
    }));
  }
  public declineAssignment(assignmentId: string): void {
    this.subscriptions.push(this.assignmentService.declineAssignment(assignmentId).subscribe(() => {
      this.loadAssignments();
    }));
  }

  public updateAssignments(): void {
    this.loadAssignments();
  }

  public openModal(template: TemplateRef<any>, assignment: Assignment): void {
    if (assignment) {
      this.editMode = true;
      this.editableAssignment = Assignment.cloneAssignment(assignment);
    } else {
      this.refreshAssignment();
      this.editMode = false;
    }
    this.modalRef = this.modalService.show(template);
  }

  public closeModal(): void {
    this.modalRef.hide();
  }

  public showStatistics() {
    this.subscriptions.push(this.assignmentService.getAssignments().subscribe(assignments=> {
      this.assignments = assignments as Assignment[];
      let applied = 0;
      let declined = 0;
      for (let assignment of this.assignments) {
        if (assignment.status=="APPLIED") {
          applied++;
        } else if (assignment.status=="DECLINED") {
          declined++;
        }
      }
      this.appliedAssignments = applied;
      this.declinedAssignments = declined;
    }));
    this.statisticsMode = true;
  }


  private loadAssignments(): void {
    this.subscriptions.push(this.assignmentService.getAssignments().subscribe(assignments=> {
      this.assignments = assignments as Assignment[];
    }));

  }

  private refreshAssignment(): void {
    this.editableAssignment = new Assignment();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
