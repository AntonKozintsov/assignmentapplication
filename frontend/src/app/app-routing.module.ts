import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./modules/home/home.component";
import {NotFoundComponent} from "./modules/not-found/not-found.component";
import {ClientAssignmentsComponent} from "./modules/client-assignments/client-assignments.component";

const routes: Routes = [
  {path: 'assignments/client', component: ClientAssignmentsComponent },
  {path: 'assignments/admin', component: HomeComponent},
  {path: '', redirectTo: '/assignments/admin', pathMatch: 'full' },
  {path: '**', component: NotFoundComponent }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
