package com.anton.administrator.converter;

import com.anton.administrator.entity.Assignment;
import com.anton.administrator.view_model.AssignmentModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AssignmentConverter {

    public AssignmentModel toModel(Assignment assignment) {
        return new AssignmentModel(
                assignment.getId(), assignment.getRequest(),
                assignment.getBid(), assignment.getDue_date(),
                assignment.getStatus()
        );
    }

    public List<AssignmentModel> toModel(List<Assignment> assignmentList) {
        return assignmentList.stream().map(this::toModel)
                .collect(Collectors.toList());
    }

    public Assignment toEntity(AssignmentModel assignmentModel) {
        return new Assignment(
                assignmentModel.getId(),
                assignmentModel.getRequest(), assignmentModel.getBid(),
                assignmentModel.getDue_date(), assignmentModel.getStatus()
        );
    }
}
