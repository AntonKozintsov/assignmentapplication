package com.anton.administrator.repository;

import com.anton.administrator.entity.Assignment;
import com.anton.administrator.entity.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AssignmentRepository extends CrudRepository<Assignment, Long> {

    Assignment findAssignmentById(Long id);

    List<Assignment> findAssignmentsByStatus(Status status);

}
