package com.anton.administrator.entity;

public enum Status {

    OPENED,

    APPLIED,

    DECLINED;
}
