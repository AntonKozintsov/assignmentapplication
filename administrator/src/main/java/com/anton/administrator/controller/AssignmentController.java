package com.anton.administrator.controller;

import com.anton.administrator.entity.Status;
import com.anton.administrator.service.AssignmentService;
import com.anton.administrator.view_model.AssignmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/assignments")
public class AssignmentController {

    private final AssignmentService assignmentService;

    @Autowired
    public AssignmentController(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    @GetMapping("")
    public List<AssignmentModel> getAssignments() {
        return assignmentService.findAll();
    }

    @GetMapping("/applied")
    public List<AssignmentModel> getAppliedAssignments() {
        return assignmentService.assignmentsStatistics(Status.APPLIED);
    }

    @GetMapping("/declined")
    public List<AssignmentModel> getDeclinedAssignments() {
        return assignmentService.assignmentsStatistics(Status.DECLINED);
    }

    @PostMapping("/save")
    public AssignmentModel saveAssignment(@RequestBody AssignmentModel assignmentModel) {
        return assignmentService.save(assignmentModel);
    }

    @PostMapping("/apply/{assignmentId}")
    public AssignmentModel applyAssignment(@PathVariable Long assignmentId) {
        return assignmentService.updateAssignment(assignmentId, Status.APPLIED);
    }

    @PostMapping("/decline/{assignmentId}")
    public AssignmentModel declineAssignment(@PathVariable Long assignmentId) {
        return assignmentService.updateAssignment(assignmentId, Status.DECLINED);
    }

    @DeleteMapping("/delete/{assignmentId}")
    public void deleteAssignment(@PathVariable Long assignmentId) {
        assignmentService.deleteAssignment(assignmentId);
    }
}
