package com.anton.administrator.service;

import com.anton.administrator.entity.Status;
import com.anton.administrator.view_model.AssignmentModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AssignmentService {

    @Transactional
    List<AssignmentModel> findAll();

    @Transactional
    AssignmentModel updateAssignment(Long id, Status status);

    @Transactional
    List<AssignmentModel> assignmentsStatistics(Status status);

    @Transactional
    void deleteAssignment(Long id);

    @Transactional
    AssignmentModel save(AssignmentModel assignmentModel);

}
