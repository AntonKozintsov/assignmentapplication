package com.anton.administrator.service.impl;

import com.anton.administrator.converter.AssignmentConverter;
import com.anton.administrator.entity.Assignment;
import com.anton.administrator.entity.Status;
import com.anton.administrator.repository.AssignmentRepository;
import com.anton.administrator.service.AssignmentService;
import com.anton.administrator.view_model.AssignmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    private final AssignmentRepository assignmentRepository;

    private final AssignmentConverter assignmentConverter;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository, AssignmentConverter assignmentConverter) {
        this.assignmentRepository = assignmentRepository;
        this.assignmentConverter = assignmentConverter;
    }

    @Override
    public List<AssignmentModel> findAll() {
        return assignmentConverter.toModel((List<Assignment>) assignmentRepository.findAll());
    }

    @Override
    public AssignmentModel updateAssignment(Long id, Status status) {
        Assignment assignment = assignmentRepository.findAssignmentById(id);
        assignment.setStatus(status);
        assignmentRepository.save(assignment);
        return assignmentConverter.toModel(assignment);
    }

    @Override
    public List<AssignmentModel> assignmentsStatistics(Status status) {
        return assignmentConverter.toModel(assignmentRepository.findAssignmentsByStatus(status));
    }

    @Override
    public void deleteAssignment(Long id) {
        assignmentRepository.deleteById(id);
    }

    @Override
    public AssignmentModel save(AssignmentModel assignmentModel) {
        return assignmentConverter.toModel(assignmentRepository.save(assignmentConverter.toEntity(assignmentModel)));
    }
}
