package com.anton.client.view_model;

import com.anton.client.entity.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor()
@NoArgsConstructor
public class AssignmentModel {

    private Long id;

    private String request;

    private BigDecimal bid;

    private LocalDate due_date;

    private Status status;

}
