package com.anton.client.controller;

import com.anton.client.service.AssignmentService;
import com.anton.client.view_model.AssignmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/assignments")
public class AssignmentController {

    private final AssignmentService assignmentService;

    @Autowired
    public AssignmentController(AssignmentService assignmentService) {
        this.assignmentService = assignmentService;
    }

    @GetMapping("")
    public List<AssignmentModel> getAssignments() {
        return assignmentService.findAll();
    }

    @PostMapping("/save")
    public AssignmentModel createAssignment(@RequestBody AssignmentModel assignmentModel) {
        return assignmentService.save(assignmentModel);
    }

    @GetMapping("/status/{assignmentId}")
    public AssignmentModel getStatus(@PathVariable Long assignmentId) {
        return assignmentService.getStatus(assignmentId);
    }
}
