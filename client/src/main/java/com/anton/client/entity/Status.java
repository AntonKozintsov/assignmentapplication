package com.anton.client.entity;

public enum Status {

    OPENED,

    APPLIED,

    DECLINED;
}
