package com.anton.client.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@NoArgsConstructor
public class Assignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String request;

    private BigDecimal bid;

    private LocalDate due_date;

    @Enumerated(EnumType.STRING)
    private Status status;

    public Assignment(String request, BigDecimal bid, LocalDate due_date, Status status) {
        this.request = request;
        this.bid = bid;
        this.due_date = due_date;
        this.status = status;
    }
}
