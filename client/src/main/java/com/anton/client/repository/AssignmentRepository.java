package com.anton.client.repository;

import com.anton.client.entity.Assignment;
import org.springframework.data.repository.CrudRepository;

public interface AssignmentRepository extends CrudRepository<Assignment, Long> {
    Assignment findAssignmentById(Long assignmentId);
}
