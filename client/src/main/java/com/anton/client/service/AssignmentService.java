package com.anton.client.service;

import com.anton.client.view_model.AssignmentModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AssignmentService {

    @Transactional
    AssignmentModel save(AssignmentModel assignmentModel);

    @Transactional
    AssignmentModel getStatus(Long assignmentId );

    @Transactional
    List<AssignmentModel> findAll();
}
