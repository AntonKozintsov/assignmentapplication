package com.anton.client.service.impl;

import com.anton.client.converter.AssignmentConverter;
import com.anton.client.entity.Assignment;
import com.anton.client.entity.Status;
import com.anton.client.repository.AssignmentRepository;
import com.anton.client.service.AssignmentService;
import com.anton.client.view_model.AssignmentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AssignmentServiceImpl implements AssignmentService {

    private final AssignmentRepository assignmentRepository;

    private final AssignmentConverter assignmentConverter;

    @Autowired
    public AssignmentServiceImpl(AssignmentRepository assignmentRepository, AssignmentConverter assignmentConverter) {
        this.assignmentRepository = assignmentRepository;
        this.assignmentConverter = assignmentConverter;
    }

    @Override
    public AssignmentModel save(AssignmentModel assignmentModel) {
        Assignment assignment = assignmentConverter.toEntity(assignmentModel);
        assignment.setStatus(Status.OPENED);
        return assignmentConverter.toModel(assignmentRepository.save(assignment));
    }

    @Override
    public AssignmentModel getStatus(Long assignmentId) {
        return assignmentConverter.toModel(assignmentRepository.findAssignmentById(assignmentId));
    }

    @Override
    public List<AssignmentModel> findAll() {
        return assignmentConverter.toModel((List<Assignment>) assignmentRepository.findAll());
    }

}
