CREATE SCHEMA IF NOT EXISTS application_db;
INSERT INTO application_db.assignment (id, bid, due_date, request, status) VALUES (1, 25.00, '2019-04-14', 'some information', 'OPENED');
INSERT INTO application_db.assignment (id, bid, due_date, request, status) VALUES (2, 30.00, '2019-04-16', 'walk a dog', 'OPENED');
INSERT INTO application_db.assignment (id, bid, due_date, request, status) VALUES (3, 20.00, '2019-04-26', 'complete repository implementation', 'DECLINED');
INSERT INTO application_db.assignment (id, bid, due_date, request, status) VALUES (4, 40.00, '1970-01-23', 'complete front-end development', 'APPLIED');
